package com.gitlab.gerhard_piette.utf_6;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.letter_1.LetterEnd;
import com.gitlab.gerhard_piette.letter_1.Letter;

/**
 * https://en.wikipedia.org/wiki/UTF-8
 */
public class Utf8 implements Letter {

	//public final static int max1 = 0b01111111;
	public final static int MAX_X = 0b10111111;
	//public final static int max2 = 0b11011111;
	//public final static int max3 = 0b11101111;
	//public final static int max4 = 0b11110111;

	public final static int LIM_1 = 0b10000000;
	//public final static int limx = 0b11000000;
	public final static int LIM_2 = 0b11100000;
	public final static int LIM_3 = 0b11110000;
	public final static int LIM_4 = 0b11111000;

	public final static int READ_MASK_X = 0b00111111;
	public final static int READ_MASK_2 = 0b00011111;
	public final static int READ_MASK_3 = 0b00001111;
	public final static int READ_MASK_4 = 0b00000111;

	public int readUnit(byte[] data, int offset) {
		int ret = data[offset] & 0xFF;
		return ret;
	}

	/**
	 * https://en.wikipedia.org/wiki/Byte_order_mark#UTF-8
	 *
	 * @param data
	 * @return
	 */
	public boolean checkBOM(byte[] data) {
		if (data.length < 3) {
			return false;
		}
		var int1 = readUnit(data, 0);
		if (int1 != 0xEF) {
			return false;
		}
		var int2 = readUnit(data, 1);
		if (int2 != 0xBB) {
			return false;
		}
		var int3 = readUnit(data, 2);
		if (int3 != 0xBF) {
			return false;
		}
		return true;
	}

	/**
	 * Does NOT check if the codepoint is valid.
	 */
	@Override
	public LetterEnd read(byte[] data, int offset) throws DefectLetter, DefectOffset {
		int rem = data.length - offset;
		if (rem == 0) {
			throw new DefectOffset(data.length, offset);
		}
		//1 byte codepoint.
		int byte1 = readUnit(data, offset);
		if (byte1 < LIM_1) {
			int number = byte1;
			return new LetterEnd(number, offset + 1);
		}
		//2 byte codepoint.
		if (rem < 2) {
			throw new DefectOffset(data.length, offset + 1);
		}
		int byte2 = readUnit(data, offset + 1);
		if (byte1 < LIM_2) {
			if (byte2 > MAX_X) {
				throw new DefectLetter(offset);
			}
			int number = ((READ_MASK_2 & byte1) << 6) | (READ_MASK_X & byte2);
			return new LetterEnd(number, offset + 2);
		}
		//3 byte codepoint.
		if (rem < 3) {
			throw new DefectOffset(data.length, offset + 2);
		}
		int byte3 = readUnit(data, offset + 2);
		if (byte1 < LIM_3) {
			if (byte2 > MAX_X || byte3 > MAX_X) {
				throw new DefectLetter(offset);
			}
			int number = ((READ_MASK_3 & byte1) << 12) | ((READ_MASK_X & byte2) << 6) | (READ_MASK_X & byte3);
			return new LetterEnd(number, offset + 3);
		}
		//4 byte codepoint.
		if (rem < 4) {
			throw new DefectOffset(data.length, offset + 3);
		}
		int byte4 = readUnit(data, offset + 3);
		if (byte1 < LIM_4) {
			if (byte2 > MAX_X || byte3 > MAX_X || byte4 > MAX_X) {
				throw new DefectLetter(offset);
			}
			int number = ((READ_MASK_4 & byte1) << 18) | ((READ_MASK_X & byte2) << 12) | ((READ_MASK_X & byte3) << 6) | (READ_MASK_X & byte4);
			return new LetterEnd(number, offset + 4);
		}
		// error. There is no valid 5 byte code point.
		throw new DefectLetter(offset);
	}

	public final static int CP_MAX_1 = 0x007F;
	public final static int CP_MAX_2 = 0x07FF;
	public final static int CP_MAX_3 = 0xFFFF;
	public final static int CP_MAX_4 = 0x10FFFF;//max codepoint.

	public final static int WRITE_MASK_X = 0b10000000;
	public final static int WRITE_MASK_2 = 0b11000000;
	public final static int WRITE_MASK_3 = 0b11100000;
	public final static int WRITE_MASK_4 = 0b11110000;

	/**
	 * The first bytes of the data array are written.
	 * An array and not an integer is used because encoding is typically used to create an array of bytes.
	 * <p>
	 * Checks if the codepoint is valid.
	 *
	 * @return The number of bytes written.
	 */
	@Override
	public int write(byte[] data, int offset, int letter) throws DefectLetter, DefectOffset {
		int cp = letter;
		if (Utf.isValidCodepoint(cp)) {
			if (cp <= CP_MAX_1) {
				if (offset + 1 > data.length) {
					throw new DefectOffset(data.length, offset);
				}
				data[offset] = (byte) cp;
				return 1;
			}
			if (cp <= CP_MAX_2) {
				if (offset + 2 > data.length) {
					throw new DefectOffset(data.length, offset + 1);
				}
				data[offset] = (byte) (WRITE_MASK_2 | (cp >> 6));
				data[offset + 1] = (byte) (WRITE_MASK_X | (READ_MASK_X & cp));
				return 2;
			}
			if (cp <= CP_MAX_3) {
				if (offset + 3 > data.length) {
					throw new DefectOffset(data.length, offset + 2);
				}
				data[offset] = (byte) (WRITE_MASK_3 | (cp >> 12));
				data[offset + 1] = (byte) (WRITE_MASK_X | (READ_MASK_X & (cp >> 6)));
				data[offset + 2] = (byte) (WRITE_MASK_X | (READ_MASK_X & cp));
				return 3;
			}
			if (cp <= CP_MAX_4) {
				if (offset + 4 > data.length) {
					throw new DefectOffset(data.length, offset + 3);
				}
				data[offset] = (byte) (WRITE_MASK_4 | (cp >> 18));
				data[offset + 1] = (byte) (WRITE_MASK_X | (READ_MASK_X & (cp >> 12)));
				data[offset + 2] = (byte) (WRITE_MASK_X | (READ_MASK_X & (cp >> 6)));
				data[offset + 3] = (byte) (WRITE_MASK_X | (READ_MASK_X & cp));
				return 4;
			}
		}
		throw new DefectLetter(0);
	}
}
