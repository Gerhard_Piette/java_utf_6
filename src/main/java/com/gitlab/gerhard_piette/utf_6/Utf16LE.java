package com.gitlab.gerhard_piette.utf_6;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;

/**
 * UTF-16 little endian.
 * https://en.wikipedia.org/wiki/UTF-16
 * https://golang.org/src/unicode/utf16/utf16.go
 */
public class Utf16LE extends Utf16 {

	@Override
	public int readUnit(byte[] data, int offset) {
		int b0 = data[offset] & 0xFF;
		int b1 = data[offset + 1] & 0xFF;
		int ret = (b1 << 8) | b0;
		return ret;
	}

	/**
	 * The first bytes of the data array are written.
	 * An array and not an integer is used because encoding is typically used to create an array of bytes.
	 * <p>
	 * Checks if the codepoint is valid.
	 *
	 * @return The number of bytes written.
	 */
	@Override
	public int write(byte[] data, int offset, int letter) throws DefectLetter, DefectOffset {
		int cp = letter;
		if (Utf.isValidCodepoint(cp)) {
			if (cp < 0x10000) {
				if (offset + 2 > data.length) {
					throw new DefectOffset(data.length, offset + 1);
				}
				// Codepoint of 1 unit (2 bytes).
				data[offset + 1] = (byte) (cp >> 8);
				data[offset] = (byte) cp;
				return 2;
			} else {
				if (offset + 4 > data.length) {
					throw new DefectOffset(data.length, offset + 3);
				}
				// Codepoint of 2 units (4 bytes).
				int x = cp - 0x10000;
				int part1 = 0xD800 + (MASK_10 & (x >> 10));
				data[offset + 1] = (byte) (part1 >> 8);
				data[offset] = (byte) part1;
				int part2 = 0xDC00 + (MASK_10 & x);
				data[offset + 3] = (byte) (part2 >> 8);
				data[offset + 2] = (byte) part2;
				return 4;
			}
		}
		throw new DefectLetter(0);
	}

}
