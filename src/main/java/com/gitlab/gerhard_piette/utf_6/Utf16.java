package com.gitlab.gerhard_piette.utf_6;


import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.letter_1.LetterEnd;
import com.gitlab.gerhard_piette.letter_1.Letter;

/**
 * UTF-16 big endian.
 * https://en.wikipedia.org/wiki/UTF-16
 * https://golang.org/src/unicode/utf16/utf16.go
 */
public class Utf16 implements Letter {

	public static final int BOM_16 = 0xFEFF;

	public static final int BAD_BOM_16 = 0xFFFE;

	public int readUnit(byte[] data, int offset) {
		int b0 = data[offset] & 0xFF;
		int b1 = data[offset + 1] & 0xFF;
		int ret = (b0 << 8) | b1;
		return ret;
	}

	/**
	 * Does NOT check if the codepoint is valid.
	 */
	@Override
	public LetterEnd read(byte[] data, int offset) throws DefectLetter, DefectOffset {
		int rem = data.length - offset;
		if (rem < 2) {
			throw new DefectOffset(data.length, offset);
		}
		int unit1 = readUnit(data, offset);
		// Codepoint of 1 unit (2 bytes).
		if ((unit1 < Utf.INVALID_MIN) || (unit1 > Utf.INVALID_MAX)) {
			return new LetterEnd(unit1, 2);
		}
		// Codepoint of 2 units (4 bytes).
		if (rem < 4) {
			throw new DefectOffset(data.length, offset + 2);
		}
		int part1 = (unit1 - 0xD800) << 10;
		int unit2 = readUnit(data, offset + 2);
		int part2 = unit2 - 0xDC00;
		int number = part1 + part2 + 0x10000;
		return new LetterEnd(number, 4);
	}


	/**
	 * https://en.wikipedia.org/wiki/Modulo_operation
	 * x % 2n == x & (2n - 1)
	 * x % 0x400 == x & (0x400 - 1) = x & 0x3FF
	 */
	public static final int MASK_10 = 0x3FF;//0b1111111111

	/**
	 * The first bytes of the data array are written.
	 * An array and not an integer is used because encoding is typically used to create an array of bytes.
	 * <p>
	 * Checks if the codepoint is valid.
	 *
	 * @return The number of bytes written.
	 */
	@Override
	public int write(byte[] data, int offset, int letter) throws DefectLetter, DefectOffset, DefectLetter {
		int cp = letter;
		if (Utf.isValidCodepoint(cp)) {
			if (cp < 0x10000) {
				if (offset + 2 > data.length) {
					throw new DefectOffset(data.length, offset + 1);
				}
				// Codepoint of 1 unit (2 bytes).
				data[offset] = (byte) (cp >> 8);
				data[offset + 1] = (byte) cp;
				return 2;
			} else {
				if (offset + 4 > data.length) {
					throw new DefectOffset(data.length, offset + 3);
				}
				// Codepoint of 2 units (4 bytes).
				int x = cp - 0x10000;
				int part1 = 0xD800 + (MASK_10 & (x >> 10));
				data[offset] = (byte) (part1 >> 8);
				data[offset + 1] = (byte) part1;
				int part2 = 0xDC00 + (MASK_10 & x);
				data[offset + 2] = (byte) (part2 >> 8);
				data[offset + 3] = (byte) part2;
				return 4;
			}
		}
		throw new DefectLetter(0);
	}

}
