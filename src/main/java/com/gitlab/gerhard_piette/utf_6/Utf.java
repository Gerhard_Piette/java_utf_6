package com.gitlab.gerhard_piette.utf_6;

public class Utf {

	//https://en.wikipedia.org/wiki/UTF-8
	public static final int INVALID_MIN = 0xD800;
	public static final int INVALID_MAX = 0xDFFF;//0xE000 - 1
	public static final int MAX_CODEPOINT = 0x10FFFF;

	public static final boolean isValidCodepoint(int cp) {
		if (cp >= 0 && cp <= MAX_CODEPOINT) {
			if ((cp < INVALID_MIN) || (cp > INVALID_MAX)) {
				return true;
			}
		}
		return false;
	}

}