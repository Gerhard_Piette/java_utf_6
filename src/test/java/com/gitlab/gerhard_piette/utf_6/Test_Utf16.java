package com.gitlab.gerhard_piette.utf_6;

import org.junit.jupiter.api.Test;
import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * https://en.wikipedia.org/wiki/UTF-16
 */
public class Test_Utf16 {

	Utf16 utf16 = new Utf16();

	@Test
	public void checkBOM() throws DefectLetter, DefectOffset {
		var data = new byte[]{(byte) 0xFE, (byte) 0xFF};
		var unit = utf16.readUnit(data, 0);
		assertEquals(0xFEFF, unit);
	}

	@Test
	public void decode1() throws DefectLetter, DefectOffset {
		var data = new byte[]{0x00, 0x24};
		var nl = utf16.read(data, 0);
		assertEquals(0x24, nl.letter);
	}

	@Test
	public void decode2() throws DefectLetter, DefectOffset {
		var data = new byte[]{(byte) 0x20, (byte) 0xAC};
		var nl = utf16.read(data, 0);
		assertEquals(0x20AC, nl.letter);
	}

	@Test
	public void decode3() throws DefectLetter, DefectOffset {
		var data = new byte[]{(byte) 0xD8, (byte) 0x01, (byte) 0xDC, (byte) 0x37};
		var nl = utf16.read(data, 0);
		assertEquals(0x10437, nl.letter);
	}

	@Test
	public void decode4() throws DefectLetter, DefectOffset {
		var data = new byte[]{(byte) 0xD8, (byte) 0x52, (byte) 0xDF, (byte) 0x62};
		var nl = utf16.read(data, 0);
		assertEquals(0x24B62, nl.letter);
	}

	@Test
	public void encode1() throws DefectLetter, DefectOffset {
		var data = new byte[]{0x00, 0x24};
		var nl = utf16.read(data, 0);
		assertEquals(0x24, nl.letter);
		var data2 = new byte[4];
		utf16.write(data2, 0, nl.letter);
		for (int off = 0; off < data.length; off++) {
			assertEquals(data[off], data2[off]);
		}
	}

	@Test
	public void encode2() throws DefectLetter, DefectOffset {
		var data = new byte[]{(byte) 0x20, (byte) 0xAC};
		var nl = utf16.read(data, 0);
		assertEquals(0x20AC, nl.letter);
		var data2 = new byte[4];
		utf16.write(data2, 0, nl.letter);
		for (int off = 0; off < data.length; off++) {
			assertEquals(data[off], data2[off]);
		}
	}

	@Test
	public void encode3() throws DefectLetter, DefectOffset {
		var data = new byte[]{(byte) 0xD8, (byte) 0x01, (byte) 0xDC, (byte) 0x37};
		var str = new String(data);
		var nl = utf16.read(data, 0);
		assertEquals(0x10437, nl.letter);
		var data2 = new byte[4];
		utf16.write(data2, 0, nl.letter);
		for (int off = 0; off < data.length; off++) {
			assertEquals(data[off], data2[off]);
		}
	}

	@Test
	public void encode4() throws DefectLetter, DefectOffset {
		var data = new byte[]{(byte) 0xD8, (byte) 0x52, (byte) 0xDF, (byte) 0x62};
		var nl = utf16.read(data, 0);
		assertEquals(0x24B62, nl.letter);
		var data2 = new byte[4];
		utf16.write(data2, 0, nl.letter);
		for (int off = 0; off < data.length; off++) {
			assertEquals(data[off], data2[off]);
		}
	}


}
