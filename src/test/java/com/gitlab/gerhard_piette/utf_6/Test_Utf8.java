package com.gitlab.gerhard_piette.utf_6;

import org.junit.jupiter.api.Test;
import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;


import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * https://en.wikipedia.org/wiki/UTF-8
 */
public class Test_Utf8 {

	Utf8 utf8 = new Utf8();

	@Test
	public void checkBOM() throws DefectLetter, DefectOffset {
		var data = new byte[]{(byte) 0xEF, (byte) 0xBB, (byte) 0xBF};
		var x = utf8.checkBOM(data);
		assertEquals(true, x);
	}

	@Test
	public void decode1() throws DefectLetter, DefectOffset {
		var data = new byte[]{0x24};
		var nl = utf8.read(data, 0);
		assertEquals(0x24, nl.letter);
	}

	@Test
	public void decode2() throws DefectLetter, DefectOffset {
		var data = new byte[]{(byte) 0xC2, (byte) 0xA2};
		var nl = utf8.read(data, 0);
		assertEquals(0xA2, nl.letter);
	}

	@Test
	public void decode3() throws DefectLetter, DefectOffset {
		var data = new byte[]{(byte) 0xE0, (byte) 0xA4, (byte) 0xB9};
		var nl = utf8.read(data, 0);
		assertEquals(0x0939, nl.letter);
	}

	@Test
	public void decode4() throws DefectLetter, DefectOffset {
		var data = new byte[]{(byte) 0xE2, (byte) 0x82, (byte) 0xAC};
		var nl = utf8.read(data, 0);
		assertEquals(0x20AC, nl.letter);
	}

	@Test
	public void decode5() throws DefectLetter, DefectOffset {
		var data = new byte[]{(byte) 0xED, (byte) 0x95, (byte) 0x9C};
		var nl = utf8.read(data, 0);
		assertEquals(0xD55C, nl.letter);
	}

	@Test
	public void decode6() throws DefectLetter, DefectOffset {
		var data = new byte[]{(byte) 0xF0, (byte) 0x90, (byte) 0x8D, (byte) 0x88};
		var nl = utf8.read(data, 0);
		assertEquals(0x10348, nl.letter);
	}

	@Test
	public void encode1() throws DefectLetter, DefectOffset {
		var data = new byte[]{0x24};
		var nl = utf8.read(data, 0);
		assertEquals(nl.letter, 0x24);
		var data2 = new byte[4];
		utf8.write(data2, 0, nl.letter);
		for (int off = 0; off < data.length; off++) {
			assertEquals(data[off], data2[off]);
		}
	}

	@Test
	public void encode2() throws DefectLetter, DefectOffset {
		var data = new byte[]{(byte) 0xC2, (byte) 0xA2};
		var nl = utf8.read(data, 0);
		assertEquals(nl.letter, 0xA2);
		var data2 = new byte[4];
		utf8.write(data2, 0, nl.letter);
		for (int off = 0; off < data.length; off++) {
			assertEquals(data[off], data2[off]);
		}
	}

	@Test
	public void encode3() throws DefectLetter, DefectOffset {
		var data = new byte[]{(byte) 0xE0, (byte) 0xA4, (byte) 0xB9};
		var nl = utf8.read(data, 0);
		assertEquals(nl.letter, 0x0939);
		var data2 = new byte[4];
		utf8.write(data2, 0, nl.letter);
		for (int off = 0; off < data.length; off++) {
			assertEquals(data[off], data2[off]);
		}
	}

	@Test
	public void encode4() throws DefectLetter, DefectOffset {
		var data = new byte[]{(byte) 0xE2, (byte) 0x82, (byte) 0xAC};
		var nl = utf8.read(data, 0);
		assertEquals(nl.letter, 0x20AC);
		var data2 = new byte[4];
		utf8.write(data2, 0, nl.letter);
		for (int off = 0; off < data.length; off++) {
			assertEquals(data[off], data2[off]);
		}
	}

	@Test
	public void encode5() throws DefectLetter, DefectOffset {
		var data = new byte[]{(byte) 0xED, (byte) 0x95, (byte) 0x9C};
		var nl = utf8.read(data, 0);
		assertEquals(nl.letter, 0xD55C);
		var data2 = new byte[4];
		utf8.write(data2, 0, nl.letter);
		for (int off = 0; off < data.length; off++) {
			assertEquals(data[off], data2[off]);
		}
	}

	@Test
	public void encode6() throws DefectLetter, DefectOffset {
		var data = new byte[]{(byte) 0xF0, (byte) 0x90, (byte) 0x8D, (byte) 0x88};
		var nl = utf8.read(data, 0);
		assertEquals(nl.letter, 0x10348);
		var data2 = new byte[4];
		int x = utf8.write(data2, 0, nl.letter);
		for (int off = 0; off < data.length; off++) {
			assertEquals(data[off], data2[off]);
		}
	}


}
